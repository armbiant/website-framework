# Template - website - documentation

This is the template for building a docs website.

The steps needed to create this repo are mentionned here to help you get started from scratch.

You can also decide to clone this repo to get started with a template project.

## License

This work is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.

## Note on git commit

You can follow along this tutorial through the git history of the repo. The checkpoints are there to help you find the relevant changes.

## Steps to recreate this repo from scratch

[[_TOC_]]

## get started: install sphinx, do a sphinx-quickstart

First, we are going to create a directory to have a home for the project.

    mkdir template-website

    cd template-website

Then, we are going to set up git to enable version control on our files.

    git init

It is recommended to install everything in a Python virtualenv. Let's create one:

    python3 -m virtualenv venv

We need to activate the virtualenv:

    source venv/bin/activate
    
Add `venv/*` to the `.gitignore` file so that this folder is not tracked by `git`.

We are now ready to install Sphinx.

    pip install -U Sphinx

Let's get started on the website by using the `sphinx-quickstart` command:

    sphinx-quickstart
    
It will ask you a few questions about your project. You can answer with placeholder information if this is a test.

Finally, add `build/*`, `*.bat` to .gitignore.

## first checkpoint: make a commit

Let's make a git commit to keep track of the progress made.

## add sphinx-intl

We would like to have multiple locales for the website. We do this with `sphinx-intl`.

Let's install it in our virtualenv.

    pip install sphinx-intl

To enable locales, we need to mention some information in the `source/conf.py` file. Add these lines to the file:

    locale_dirs = ['locale/']   # path is example but recommended.
    gettext_compact = False     # optional.

We can now extract messages from the `.rst` files that were created by `sphinx-quickstart`:

    make gettext

Do not forget to update `.gitignore` so that it ignores `*.mo` files.

We will now generate the `.po` files associated with the `.rst` files. The `-l` option allows us to select the locale:

    sphinx-intl update -p build/gettext -l fr

*Optionnal: In the current project, we translated one message from En->Fr as a way to keep track of versions and to double-check that everything is working fine. You don't have to do this for now.*

## second checkpoint

Let's make a git commit!

## install Furo theme

We now want to personalize the website with the Furo theme.

First, install the theme inside the virtualenv:

    pip install furo

Now, update `source/conf.py` with the new configuration:

    html_theme = "furo"

## checkpoint 3

Let's make a git commit!

## add the landing page

We are now ready to further customize the docs so that it fits what is currently in use at the Metalab. All the files mentioned are available in this repo.

* Copy custom_landing_page.css to `source/_static/css`

* Update custom_landing_page.css to suit your colors need

* Copy furo_page.html to `source/_templates`

* Copy index.html to `source/_templates`

* Copy page.html to `source/_templates`
              
* Copy index.html to `source/`

* Copy index.rst to `source/`

We will now generate the `.pot` files associated with the landing page:

    make gettext

The `.pot` files will be used to generate `.po` files, used as the base for localization:

    sphinx-intl update -p build/gettext -l fr

We need to tell Sphinx what is the name of the page that will be used to create the `index.html` through the docs, since we are using `index.html` for the landing page. Update `source/conf.py` with:

     master_doc = 'contents'

* Copy contents.rst to `source/*`

Now you should be able to `sphinx-build` your website! Let's try it:

    sphinx-build -b html -D language=en source public/en
    sphinx-build -b html -D language=fr source public/fr
    
The files are ready to be viewed in a web browser. You may start a `localhost` server with Python:

    python3 -m http.server

### checkpoint 4

Let's commit our changes.

## restrict the custom CSS to the landing page

you might need to tweak the css tag names and to update the index.rst sections to prevent the css from the landing page to leak onto the docs... will make an issue for this - I managed to make it work in SATIE... TODO

## update the CI

We will update the CI so that we can deploy the website to Gitlab Pages:

    pip freeze > requirements.txt

We will *update the Makefile* so that it contains the commands used by the Gitlab CI. You can copy the `Makefile` in this repo into yours.

Add .gitlab-ci.yml to the root of `template-website`, so that Gitlab will be able to find it.

Add public/* to .gitignore - this is were the newly available `make doc` and `make intl` commands are.

You can now make the docs with your local changes by using `make intl` and `make doc`. If you are happy with the result, you can push them to Gitlab to see your website live - it may take a few minutes.


## checkpoint 5

We are done for now - let's commit this work :)

# further steps

we would need to update this template to make it able to switch between versions and locales at anytime in the docs.

let's start with the locales menu

## locales menu

### initialize the locale menu

inspiration from https://stackoverflow.com/a/63054018

_in `source/_templates/`, create a `sidebar` folder and add the `navigation.html` file to it._

_in `source/conf.py`, uncomment these three lines at the beginning of the file:

```python

import os
import sys
sys.path.insert(0, os.path.abspath('.'))

```

_in `source/conf.py`, add the following lines:_

```python

# -- LOCALE MENU -------------------------------------------------------------

# POPULATE LINKS TO OTHER LANGUAGES

try:
   html_context
except NameError:
   html_context = dict()

# SET CURRENT_LANGUAGE
if 'current_language' in os.environ:
   # get the current_language env var set by buildDocs.sh
   current_language = os.environ['current_language']
else:
   # the user is probably doing `make html`
   # set this build's current language to english
   current_language = 'en'
 
# tell the theme which language to we're currently building
html_context['current_language'] = current_language

html_context['languages'] = []
html_context['languages'].append( ('en', '/documentations/template-website/' + 'en' + '/') )
 
languages = [lang.name for lang in os.scandir('locale') if lang.is_dir()]
for lang in languages:
   html_context['languages'].append( (lang, '/documentations/template-website/' + lang + '/') )

```

try to build the docs with:

    make intl

    make doc

you should now have the files in the `public/` folder, with the locale version menu in the leftbar menu.

### update the locale menu to the new locale menu version

after having initialized the locale menu, we are going to make a few changes so that we can build the locale menu in differents environments.

this is based on [MR 51 in Splash repo](https://gitlab.com/sat-metalab/documentations/splash/-/merge_requests/51).

* in `source/_templates/sidebar/navigation.html`, we want to add a baseurl variable to the line defining the slug.

the complete end result should be:

```
<div class="sidebar-tree">
  {{ furo_navigation_tree }}
</div>


{% block menu %}

<div class="rst-versions" data-toggle="rst-versions" role="note" aria-label="versions">

    <div class="rst-other-versions">
    {% if languages|length >= 1 %}
    <dl>
        <dt>{{ _('Languages') }}</dt>
            {% for slug, url in languages %}
                <dd><a href="{{ baseurl ~ url ~ pagename ~ '.html'}}">{{ slug }}</a></dd>
            {% endfor %} 
    </dl>
    {% endif %}

    </div>
</div>

{% endblock %}
```
* in `source/conf.py`, we will delete the `documentations/template-website` lines:

```
# tell the theme which language to we're currently building
html_context['current_language'] = current_language

html_context['languages'] = []
html_context['languages'].append( ('en', 'en/') )
 
languages = [lang.name for lang in os.scandir('locale') if lang.is_dir()]
for lang in languages:
   html_context['languages'].append( (lang, lang + '/') )
```
* in Makefile, we are going to define new targets:

```
htmlgitlabdocs:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./documentations/template-website/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./documentations/template-website/' source public/fr
	cp source/index.html public/
	
htmllocaltest:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./public/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./public/' source public/fr
	cp source/index.html public/

htmlgitlab:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./template-website/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./template-website/' source public/fr
	cp source/index.html public/
```

* in `gitlab-ci.yml`, we are going to use the target with the appropriate baseurl for deploying on gitlab. the end result should be:

```
pages:
  stage: deploy
  image: python:3.8
  script:
  - pip install -U sphinx
  - pip install -r requirements.txt
  - make htmlgitlabdocs
  artifacts:
    paths:
    - public
  only:
  - main
```

## repair the search feature

Still investigating on if this is the best way to do this - comments welcome

### create a `source/_templates/search.html` file

Create the `source/_templates/search.html` file containing the following:

```
{% extends "furo_page.html" %}

{%- block regular_scripts -%}
{{ super() }}
<script src="{{ pathto('_static/searchtools.js', 1) }}"></script>
<script src="{{ pathto('_static/language_data.js', 1) }}"></script>
{%- endblock regular_scripts-%}

{%- block htmltitle -%}
<title>{{ _("Search") }} - {{ docstitle }}</title>
{%- endblock htmltitle -%}

{% block content %}
<noscript>
<div class="admonition error">
  <p class="admonition-title">{% trans %}Error{% endtrans %}</p>
  <p>
    {% trans %}Please activate JavaScript to enable the search functionality.{% endtrans %}
  </p>
</div>
</noscript>

<div id="search-results"></div>
{% endblock %}

{% block scripts -%}
{{ super() }}
<script src="{{ pathto('searchindex.js', 1) }}"></script>
{%- endblock scripts %}
```

### update the `source/_templates_page.html` file

We are going to tell it to use the `search.html` file we just created.

It should now look like this:

```
{% if pagename == 'index' %}
    {% include 'index.html' %}
{% elif pagename == 'search' %}
    {% include 'search.html' %}
{% else %}
    {% include 'furo_page.html' %}
{% endif %}
```

### add logo and favicon

In `_static/images`, you can find an empty logo, named `test_nologo.png`.
You will also find a `favicon.ico` in `_static/`.

#### using the logo

in `source/conf.py`, add :

    html_logo = "_static/images/test_nologo.png"

in `source/_templates/index.html`, add this line after the `<div id="content">` tag:

    <p id="logo"><img src="_static/images/test_nologo.png" alt="test - nologo" width="120" /></p>

#### using the favicon

in `source/conf.py`, add:

    html_favicon = '_static/favicon.ico'


## TODO

TODO: tweak CSS so that it fits in with the Furo theme.



